#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "shared.h"

int connect_to_master(int node_weight);
int calculate_result(int *inputs, int input_size);
int is_prime(int n);

int
main(int argc, char *argv[])
{
	int sock;
	int *inputs;
	int input_length;
	size_t input_size;
	size_t allocated_inputs_memory;
	int result;
	int task_number;
	char out_string[128];
	char in_string[4];
	ssize_t bytes_read;
	int done;
	clock_t start_calc_time, stop_calc_time;
	int node_weight;
	

	if (argc != 2) {
		printf("Usage: %s weight\n", argv[0]);
		exit(EXIT_SUCCESS);
	} else {
		node_weight = atoi(argv[1]);
	}       
	
	allocated_inputs_memory = 1024 * sizeof(int); /* start of with some memory */
	inputs = calloc(1024, sizeof(int));

	sock = connect_to_master(node_weight);

	done = 0;
	while (!done) {
		send(sock, "READY", 6, 0);

		bytes_read = recv_int(sock, &task_number);
		if (bytes_read <= 0) {
			done = 1;
			continue;
		}
			
		recv_int(sock, &input_length);
		input_size = input_length * sizeof(int);
		if (input_size > allocated_inputs_memory) {
			inputs = realloc(inputs, input_size);
			memset(inputs, 0, input_size);
		}

		recv_array(sock, inputs, input_size);

		start_calc_time = clock();
		result = calculate_result(inputs, input_length);
		stop_calc_time = clock();
		
		/* fake a delay to simulate a slow node. The sleep time is 
		 * equal to the weight times the actual calculation time */
		usleep((stop_calc_time - start_calc_time) * (node_weight-1) * 1000000 / CLOCKS_PER_SEC);
		
		sprintf(out_string, "RESULT %d %d", task_number, result);
		send_array(sock, out_string, strlen(out_string)+1);

		recv_array(sock, &in_string, 4);
		if (strncmp("NEXT", in_string, 4) != 0) {
			done = 1;
		}
	}

	return 0;
}

int
connect_to_master(int node_weight)
{
	int sock;
	int connect_res;
	struct sockaddr_in server;

	sock = socket(AF_INET, SOCK_STREAM, 0);
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons(LOADBALANCER_PORT);
	
	connect_res = connect(sock, (struct sockaddr *)&server, sizeof(server));
	if (connect_res != 0) {
		printf("Error connecting to server");
		exit(EXIT_FAILURE);
	}

	send_int(sock, node_weight);
	
	return sock;
}

int
calculate_result(int *inputs, int input_size)
{
	int i, prime_count;

	for (i = 0, prime_count = 0; i < input_size; i++) {
		if (is_prime(inputs[i])) {
			prime_count++;
		}
	}
	
	return prime_count;
}

int
is_prime(int p)
{
	int n;
	if (p < 2)
		return 0;

	for (n = 2; n*n <= p; n++) {
		if (p % n == 0)
			return 0;
	}
	return 1;
}
