#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "shared.h"

struct settings {
	int node_count;
	int task_count;
	int max_prime;
	int worst_weight;
};

struct task {
	int *inputs;
	int input_length;
	int weight; /* Weight is not really used, as it is the same as the index.. */
	int completed;
};

struct node {
	int socket;
	int weight;
	time_t wait_time_start; /* the time this node had nothing more to do */
};

enum handle_result {
	HANDLED_NOTHING,
	HANDLED_ASSIGNED,
	HANDLED_COMPLETED
};

int *distribute_work(struct node *nodes, struct task *tasks, struct settings settings);
int *generate_inputs(struct settings settings);
int  pick_task(struct task *tasks, struct settings settings, struct node node);
int  start_listener(void);
int  task_offset(int worst_weight, int task_count, int node_weight);

void assign_weights(struct task *tasks, struct settings settings);
void make_fd_set(struct node *nodes, int node_count, fd_set *sockets);

enum handle_result handle_node(struct node *node, struct task *tasks, int *results, struct settings settings);

struct node *accept_nodes(int connect_socket, struct settings *settings);
struct node  accept_node(int connect_socket);
struct settings read_settings(void);
struct task *divide_tasks(int *inputs, struct settings settings);

int
main(void)
{
	struct settings settings;
	struct task *tasks;
	struct node *nodes;
	int *inputs;
	int *results; /* an array of results for each task */
	int sock;
	int result;
	int i;
	time_t time_start, time_end, wasted_time;

	/* Read the settings for this simulation */
	settings = read_settings();

	/* Generate all the inputs to the workers */
	inputs = generate_inputs(settings);

	/* Generate all the tasks based on the inputs and the task count */
	tasks = divide_tasks(inputs, settings);

	/* Assign each task a weight based on the range of numbers it is
	 * supposed to check. Lower numers gives a smaller weight */
	assign_weights(tasks, settings);

	/* Start the master TCP socket so the worker nodes can connect */
	sock = start_listener();
	
	/* Wait for each node to connect */
	nodes = accept_nodes(sock, &settings);
	
	/* Distribute the work until everything is done */
	time_start = time(NULL);
	results = distribute_work(nodes, tasks, settings);
	time_end = time(NULL);
	
	/* Compute the final result */
	for (i = 0, result = 0; i < settings.task_count; i++) {
		result += results[i];
	}

	printf("The total number of primes between 0 and %d is %d\n",
	       settings.max_prime, result);
	printf("Computing time used in seconds: %ld\n", (time_end - time_start));

	wasted_time = 0;
	for (i = 0; i < settings.node_count; i++) {
		printf("Node %d wasted %ld seconds\n", nodes[i].socket,
		       time_end - nodes[i].wait_time_start);
		wasted_time += time_end - nodes[i].wait_time_start;
	}

	printf("Total lost computing time: %ld\n", wasted_time);
	return 0;
}

struct settings
read_settings(void)
{
	struct settings settings;
	printf("Enter the amount of nodes to simulate: ");
	scanf(" %d", &settings.node_count);
	printf("Enter the amount of tasks to create: ");
	scanf(" %d", &settings.task_count);
	printf("Enter the high limit of numbers to check: ");
	scanf(" %d", &settings.max_prime);

	return settings;
}

int *
generate_inputs(struct settings settings)
{
	int *inputs;
	int i;

	inputs = calloc(settings.max_prime, sizeof(int));

	for (i = 0; i < settings.max_prime; i++) {
		inputs[i] = i;
	}
	return inputs;
}

struct task *
divide_tasks(int *inputs, struct settings settings)
{
	struct task *tasks;
	int task_input_length;
	int i;
	
	tasks = calloc(settings.task_count, sizeof(struct task));
	task_input_length = settings.max_prime / settings.task_count;
	
	for (i = 0; i < settings.task_count; i++) {		
		tasks[i].inputs = inputs + (i * task_input_length);
		tasks[i].input_length = task_input_length;
		tasks[i].completed = 0;
	}
	
	return tasks;
}

void
assign_weights(struct task *tasks, struct settings settings)
{
	int i;

	for (i = 0; i < settings.task_count; i++) {
		tasks[i].weight = tasks[i].inputs[1];
	}      
}

int
start_listener(void)
{
	int sock;
	int enable;
	struct sockaddr_in server;
	
	sock = socket(AF_INET, SOCK_STREAM, 0);

	enable = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
		   &enable, sizeof(enable));
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(LOADBALANCER_PORT);

	bind(sock, (struct sockaddr *)&server, sizeof(server));
	listen(sock, 1);
	
	return sock;
}

struct node *
accept_nodes(int connect_socket, struct settings *settings)
{
	struct node *nodes;
	int i;
	
	nodes = calloc(settings->node_count, sizeof(struct node));

	settings->worst_weight = 0;
	printf("Accepting connections from %d nodes\n", settings->node_count);
	for (i = 0; i < settings->node_count; i++) {
		nodes[i] = accept_node(connect_socket);
		printf("Accepted %d\n", i);

		if (nodes[i].weight > settings->worst_weight)
			settings->worst_weight = nodes[i].weight;
	}
	
	return nodes;
}

struct node
accept_node(int connect_socket)
{
	struct node node;

	node.socket = accept(connect_socket, NULL, NULL);
	node.wait_time_start = 0;
	recv_int(node.socket, &node.weight);
	return node;
}

int *
distribute_work(struct node *nodes, struct task *tasks, struct settings settings)
{
	int *results;
	int i;
	int nfds;	
	fd_set sockets;
	int tasks_left;
	int ongoing_tasks;
	int ready;
	int handle_res;
	
	results = calloc(settings.task_count, sizeof(int));

	for (i = 0, nfds = 0; i < settings.node_count; i++) {
		if (nodes[i].socket > nfds)
			nfds = nodes[i].socket;
	}
	nfds++; /* The manpage says to do this */

	tasks_left = settings.task_count;
	ongoing_tasks = 0;
	while (tasks_left > 0 || ongoing_tasks > 0) {
		make_fd_set(nodes, settings.node_count, &sockets);

		/* Block until data is ready on a socket */
		ready = select(nfds, &sockets, NULL, NULL, NULL);

		for (i = 0; i < settings.node_count && ready > 0; i++) {
			if (FD_ISSET(nodes[i].socket, &sockets)) {
				handle_res = handle_node(&nodes[i], tasks,
							  results, settings);
				if (handle_res == HANDLED_COMPLETED) {
					ongoing_tasks++;
					tasks_left--;
				} else if (handle_res == HANDLED_ASSIGNED) {
					ongoing_tasks--;
				}
				ready--;
			}
		}		
	}

	return results;
}

void
make_fd_set(struct node *nodes, int node_count, fd_set *sockets)
{
	int i;

	FD_ZERO(sockets);
	for (i = 0; i < node_count; i++) {
		FD_SET(nodes[i].socket, sockets);
	}	
}

enum handle_result
handle_node(struct node *node, struct task *tasks, int *results, struct settings settings)	
{
	char read_buf[64];
	int result;
	int task_number;
	size_t input_size;

	read(node->socket, read_buf, 64);
	time(&node->wait_time_start);

	if (strncmp("READY", read_buf, 64) == 0) {
		task_number = pick_task(tasks, settings, *node);
		if (task_number == -1) {
			return HANDLED_NOTHING;
		}

		input_size = tasks[task_number].input_length * sizeof(int);
		
		send_int(node->socket, task_number);
		send_int(node->socket, tasks[task_number].input_length);
		send_array(node->socket, tasks[task_number].inputs, input_size);
		return HANDLED_ASSIGNED;
	} else if (sscanf(read_buf, "RESULT %d %d", &task_number, &result) == 2) {
		results[task_number] = result;
		send_array(node->socket, "NEXT", 4);
		return HANDLED_COMPLETED;
	} else {
		printf("Error: Got unknown data from worker: %s\n", read_buf);
		return HANDLED_NOTHING;
	}
}

int
pick_task(struct task *tasks, struct settings settings, struct node node)
{
	int i;
	int offset;

	offset = task_offset(settings.worst_weight, settings.task_count, node.weight);
	
	for (i = offset; i >= 0; i--) {
		if (tasks[i].completed == 0) {
			tasks[i].completed = 1;
			printf("Task %d [w: %d] -> node %d [w: %d]\n", i, tasks[i].weight, node.socket, node.weight);
			return i;
		}
	}

	/* All tasks below offset were completed, so try the ones above */
	for (i = offset; i < settings.task_count; i++) {
		if (tasks[i].completed == 0) {
			tasks[i].completed = 1;
			printf("Task %d [w: %d] -> node %d [w: %d]\n", i, tasks[i].weight, node.socket, node.weight);
			return i;
		}
	}

	printf("Trying to pick a task, but all tasks are completed!\n");
	return -1;
}

int
task_offset(int worst_weight, int task_count, int node_weight)
{
	int inv_weight;

	inv_weight = worst_weight - node_weight + 1;

	return task_count / worst_weight * inv_weight - 1;
}
