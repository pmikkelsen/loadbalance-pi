#include <sys/socket.h>
#include "shared.h"

int
send_int(int socket, int n)
{
	return send(socket, &n, sizeof(int), 0);
}

int
recv_int(int socket, int *n)
{
	return recv(socket, n, sizeof(int), 0);
}

int
send_array(int socket, void *buffer, size_t len)
{
	return send(socket, buffer, len, 0);
}

int
recv_array(int socket, void *buffer, size_t len)
{
	return recv(socket, buffer, len, MSG_WAITALL);
}
