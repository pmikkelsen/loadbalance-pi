#define LOADBALANCER_PORT 12345

int send_int(int socket, int n);
int recv_int(int socket, int *n);

int send_array(int socket, void *buffer, size_t len);
int recv_array(int socket, void *buffer, size_t len);

